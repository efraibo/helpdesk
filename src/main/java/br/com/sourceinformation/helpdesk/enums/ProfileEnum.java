package br.com.sourceinformation.helpdesk.enums;

public enum ProfileEnum {

    ROLE_ADMIN,
    ROLE_CUSTOMER,
    ROLE_TECHNICIAN

}
