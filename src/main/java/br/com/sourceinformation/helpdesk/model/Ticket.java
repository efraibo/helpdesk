package br.com.sourceinformation.helpdesk.model;

import br.com.sourceinformation.helpdesk.enums.PriorityEnum;
import br.com.sourceinformation.helpdesk.enums.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Getter
@Setter

@Document
public class Ticket {

    @Id
    private String id;

    @DBRef(lazy = true)
    private User user;

    private Date date;

    private String title;

    private Integer number;

    private StatusEnum status;

    private PriorityEnum priority;

    @DBRef(lazy = true)
    private User assifnedUser;

    private String description;

    private String image;

    @Transient //para não ter uma representação no BD
    private List<ChangeStatus> changes;
}

