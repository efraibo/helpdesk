package br.com.sourceinformation.helpdesk.repository;

import br.com.sourceinformation.helpdesk.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(String email);
}
